<?php

// TODO this should be a part of an install process https://devdocs.magento.com/guides/v2.0/install-gde/prereq/install-rabbitmq.html#connect-rabbitmq-to-magento-commerce, but for now this params are not working with error that --amqp-host is invalid option. MAGENTO 2.2.6

echo 'Adding amqp config' . PHP_EOL;

$ENV_PATH = getenv('MAGENTO_ROOT') . '/app/etc/env.php';

$env = require($ENV_PATH);

$env['queue'] = [
    'amqp' => [
        'host' => getenv('M2SETUP_AMQP_HOST'),
        'port' => getenv('M2SETUP_AMQP_PORT'),
        'user' => getenv('M2SETUP_AMQP_USER'),
        'password' => getenv('M2SETUP_AMQP_PASSWORD'),
    ]
];

$envData = '<?php ' . PHP_EOL . PHP_EOL . 'return ' . var_export($env, true) . ';';

file_put_contents($ENV_PATH, $envData);