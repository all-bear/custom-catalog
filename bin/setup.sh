#!/bin/sh

BASE_URL="$@"

is_installed () {
  if [ -f /lock/installed.lock ]; then
    return 1;
  else
    return 0;
  fi
}

# This will indicate that setup is in progress
touch /lock/setup.lock

cd $MAGENTO_ROOT

if is_installed; then
  # Install Magento 2 if necessary
  magento-installer

  php /tools/add-amqp-config.php
  sh /tools/module-install.sh

  magento-command cache:flush
  magento-command setup:upgrade
fi

bash /tools/update-base-url.sh "$BASE_URL"

if is_installed; then
  magento-command deploy:mode:set production
fi

rm /lock/setup.lock
touch /lock/installed.lock

>&2 echo "Starting consumer"
# We need rabbit mq some time for start up
sleep 10s
magento-command queue:consumers:start customCatalogProductUpdateConsumer