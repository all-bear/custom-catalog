#!/bin/bash

RAW_BASE_URL="$@"

normalize_path () {
  last_char="${RAW_BASE_URL: -1}"

  if [[ $last_char != "/" ]]; then
    BASE_URL="$RAW_BASE_URL/"
  else
    BASE_URL="$RAW_BASE_URL"
  fi
}

if [ -n "$RAW_BASE_URL" ]; then
  normalize_path

  >&2 echo "Changing secure/unsecure base url to $BASE_URL"

  magento-command config:set web/secure/base_url $BASE_URL
  magento-command config:set web/unsecure/base_url $BASE_URL
fi