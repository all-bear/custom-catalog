#!/bin/sh

>&2 echo "Installing Custom Catalog Module"

# Add the extension via Composer
composer config repositories.qbees '{"type": "path", "url": "/src", "options": {"symlink": false}}'

composer require "qbees/custom-catalog" "@dev"

magento-command module:enable Magento_MessageQueue
magento-command module:enable Magento_Amqp
magento-command module:enable QBees_CustomCatalog