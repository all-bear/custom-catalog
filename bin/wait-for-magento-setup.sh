#!/bin/sh

is_busy () {
  if [ -f /lock/setup.lock ]; then
      return 1;
  else
      return 0;
  fi
}

is_on_install () {
  if [ -f /lock/installed.lock ]; then
      return 0;
  else
      return 1;
  fi
}

if ! is_on_install; then
  >&2 echo "Magento installation is in progress - please wait"
  >&2 echo "You can check progress with 'docker-compose logs -f cli'"
fi

until is_on_install; do
  sleep 5
done

if ! is_busy; then
  >&2 echo "Magento setup is in progress - please wait"
  >&2 echo "You can check progress with 'docker-compose logs -f cli'"
fi

until is_busy; do
  sleep 5
done

>&2 echo "Magento setup is ready - starting up"

exec "$@"