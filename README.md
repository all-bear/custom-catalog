# Custom Catalog

## Quick Start

```bash
cp composer.env.sample composer.env
# ..put the correct tokens into composer.env

cp global.env.sample global.env
# ..change values if you need in global.env

docker-compose up magento2 # this command will install/init magento and install Custom Catalog module
```

Now it should be available at [http://magento2.local](http://magento2.local) if another url is not specified in MAGENTO_BASE_URL host env variable

## Notes

If you wanna refresh all installation you need to remove `lock`, `magento` and `data` folders, after this just follow Quick Start commands.

Image with rabbitmq management used for amqp - not sure do we need this.

For some reason on some machines you need to restart consumer after new store is created.