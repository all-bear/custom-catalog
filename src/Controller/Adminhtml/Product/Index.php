<?php

namespace QBees\CustomCatalog\Controller\Adminhtml\Product;

use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\View\Result\PageFactory;
use QBees\CustomCatalog\Controller\Adminhtml\Product;

/**
 * Class Index
 *
 * @package QBees\CustomCatalog\Controller\Adminhtml\Product
 */
class Index extends Product
{
    /**
     * @var PageFactory
     */
    private $resultPageFactory;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    ) {
        parent::__construct($context);

        $this->resultPageFactory = $resultPageFactory;
    }

    /**
     * @return Page
     */
    public function execute()
    {
        /** @var Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('QBees_CustomCatalog::custom_catalog_product');
        $resultPage->getConfig()->getTitle()->prepend(__('Products'));

        return $resultPage;
    }
}
