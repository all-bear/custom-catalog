<?php

namespace QBees\CustomCatalog\Controller\Adminhtml;

use Magento\Backend\App\Action;

/**
 * Class Product
 *
 * @package QBees\CustomCatalog\Controller\Adminhtml
 */
abstract class Product extends Action
{
    const ADMIN_RESOURCE = 'QBees_CustomCatalog::product';
}
