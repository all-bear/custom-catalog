<?php

namespace QBees\CustomCatalog\Model;

use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\MessageQueue\PublisherInterface as Publisher;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Store\Model\StoreManagerInterface as StoreManager;
use Psr\Log\LoggerInterface as Logger;
use QBees\CustomCatalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Api\ProductRepositoryInterface as CatalogProductRepository;
use QBees\CustomCatalog\Helper\Data as CoreHelper;

/**
 * Class ProductRepository
 *
 * @package QBees\CustomCatalog\Model
 */
class ProductRepository implements ProductRepositoryInterface
{
    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var CatalogProductRepository
     */
    private $productRepository;

    /**
     * @var Publisher
     */
    private $publisher;

    /**
     * @var Json
     */
    private $serializer;

    /**
     * @var StoreManager
     */
    private $storeManager;

    /**
     * @var Logger
     */
    private $logger;

    /**
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param CatalogProductRepository $productRepository
     * @param Publisher $publisher
     * @param StoreManager $storeManager
     * @param Json $serializer
     * @param Logger $logger
     */
    public function __construct(
        SearchCriteriaBuilder $searchCriteriaBuilder,
        CatalogProductRepository $productRepository,
        Publisher $publisher,
        Json $serializer,
        StoreManager $storeManager,
        Logger $logger
    ) {
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->productRepository = $productRepository;
        $this->publisher = $publisher;
        $this->serializer = $serializer;
        $this->storeManager = $storeManager;
        $this->logger = $logger;
    }

    /**
     * {@inheritdoc}
     */
    public function getByVPN($vpn)
    {
        $searchCriteria = $this->searchCriteriaBuilder
            ->addFilter(CoreHelper::VPN_ATTRIBUTE_CODE, $vpn)
            ->create();

        return $this->productRepository->getList($searchCriteria);
    }

    /**
     * {@inheritdoc}
     */
    public function updateRequest($entityId, $copywriteInfo, $vpn)
    {
        $storeId = $this->storeManager->getStore()->getId();

        $product = $this->productRepository->getById($entityId);

        $payload = [
            'data' => [
                'entity_id' => $product->getId(),
                'copywrite_info' => $copywriteInfo,
                'vpn' => $vpn
            ],
            'options' => [
                'store_id' => $storeId
            ]
        ];

        $this->publisher->publish(
            CoreHelper::PRODUCT_MESSAGE_QUEUE_TOPIC,
            $this->serializer->serialize($payload)
        );

        return $product->addData($payload['data']);
    }

    /**
     * {@inheritdoc}
     */
    public function update($payload)
    {
        $payloadData = $this->serializer->unserialize($payload);

        try {
            $productData = $payloadData['data'];
            $productId = $productData['entity_id'];
            $storeId = $payloadData['options']['store_id'];

            $product = $this->productRepository->getById($productId, false, $storeId);

            $this->productRepository->save($product->addData($productData));
        } catch (\Exception $e) {
            $this->logger->error($e, $payloadData);
        }
    }
}
