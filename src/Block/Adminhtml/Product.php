<?php

namespace QBees\CustomCatalog\Block\Adminhtml;

use Magento\Catalog\Block\Adminhtml\Product as CatalogProduct;
use QBees\CustomCatalog\Helper\Data as CoreHelper;

/**
 * Class Product
 *
 * @package QBees\CustomCatalog\Block\Adminhtml
 */
class Product extends CatalogProduct
{
    /**
     * {@inheritdoc}
     */
    protected function _getProductCreateUrl($type)
    {
        return $this->getUrl(
            'catalog/*/new',
            [
                'set' => $this->_productFactory->create()->getDefaultAttributeSetId(),
                'type' => $type,
                CoreHelper::REQUEST_PARAM_CUSTOM_CATALOG_FLAG => true
            ]
        );
    }
}