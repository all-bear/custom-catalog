<?php

namespace QBees\CustomCatalog\Block\Adminhtml\Product\Edit\Button;

use Magento\Catalog\Block\Adminhtml\Product\Edit\Button\Save as CatalogSave;
use QBees\CustomCatalog\Helper\Data as CoreHelper;

/**
 * Class Save
 *
 * @package QBees\CustomCatalog\Block\Adminhtml\Product\Edit\Button
 */
class Save extends CatalogSave
{
    const CUSTOM_CATALOG_BACK_URL = 'customcatalog/*';

    /**
     * @param array $buttonData
     *
     * @return array
     */
    public function getUIButtonDataWithCustomRedirectParam($buttonData)
    {
        $isButtonActionProvided = isset($buttonData['data_attribute']['mage-init']['buttonAdapter']['actions']);

        if ($isButtonActionProvided) {
            $buttonActions = &$buttonData['data_attribute']['mage-init']['buttonAdapter']['actions'];

            foreach ($buttonActions as &$buttonAction) {
                if (!isset($buttonAction['params'])) {
                    $buttonAction['params'] = [];
                }

                $buttonAction['params'][][CoreHelper::REQUEST_PARAM_CUSTOM_CATALOG_FLAG] = true;
            }
        }

        return $buttonData;
    }

    /**
     * {@inheritdoc}
     */
    public function getButtonData()
    {
        $isCustomCatalogReferrer = $this->context
            ->getRequestParam(CoreHelper::REQUEST_PARAM_CUSTOM_CATALOG_FLAG);

        $buttonData = parent::getButtonData();

        if (!$isCustomCatalogReferrer) {
            return $buttonData;
        }

        return $this->getUIButtonDataWithCustomRedirectParam($buttonData);
    }

    /**
     * {@inheritdoc}
     */
    protected function getOptions()
    {
        $buttonOptions = parent::getOptions();

        return array_map(function ($optionData) {
            return $this->getUIButtonDataWithCustomRedirectParam($optionData);
        }, $buttonOptions);
    }
}