# Custom Catalog

Magento 2 module, which provides custom catalog view page `Admin Panel -> Catalog -> Custom Catalog`.

Everything is under `Catalog -> Inventory -> Custom Catalog` ACL resource.

## NOTE

There are few TODO's under source, which will take some additional time to implement so I'm not sure are they important or no in current project scope.