<?php

namespace QBees\CustomCatalog\Api;

/**
 * Interface ProductRepositoryInterface
 *
 * @package QBees\CustomCatalog\Api
 */
interface ProductRepositoryInterface
{
    /**
     * @api
     * @param string $vpn
     * @return \Magento\Catalog\Api\Data\ProductSearchResultsInterface
     */
    public function getByVPN($vpn);

    /**
     * @api
     * @param int $entityId
     * @param string $copywriteInfo
     * @param int $vpn
     * @return \Magento\Catalog\Api\Data\ProductInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function updateRequest($entityId, $copywriteInfo, $vpn);

    /**
     * @param string $payload
     * @return
     */
    public function update($payload);
}
